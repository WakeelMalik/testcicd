package com.sonar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestGIt1Application {

	public static void main(String[] args) {
		SpringApplication.run(TestGIt1Application.class, args);
	}

}
